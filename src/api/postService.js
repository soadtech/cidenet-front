import { BASE_URL } from '../helpers/constants';


export default async function postService (body, route) {

    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: JSON.stringify(body),
        redirect: 'follow'
    };
    const result = await fetch(`${BASE_URL}/${route}`, requestOptions)
    const data = await result.json();
    return data
}
export async function delService (route) {

    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    const requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow'
    };
    const result = await fetch(`${BASE_URL}/${route}`, requestOptions)
    const data = await result.json();
    return data
}
export async function getService (route) {

    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    const requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };
    const result = await fetch(`${BASE_URL}/${route}`, requestOptions)
    const data = await result.json();
    return data
}
