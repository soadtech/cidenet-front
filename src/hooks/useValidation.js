import { useState, useEffect } from "react";

const useValidation = (stateInicial, validar, fn) => {
    const [values, setValues] = useState(stateInicial);
    const [errors, setErrors] = useState({});
    const [submitForm, setSubmitForm] = useState(false);

    useEffect(() => {
        if (submitForm) {
            const noErrores = Object.keys(errors).length === 0;
            if (noErrores) {
                fn();
            }
            setSubmitForm(false);
        }
    }, [errors, fn, submitForm]);

    const handleChange = (e) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        const errorsValidations = validar(values);
        setErrors(errorsValidations);
        setSubmitForm(true);
    };

    return {
        values,
        errors,
        setValues,
        submitForm,
        handleSubmit,
        handleChange
    };
};

export default useValidation;