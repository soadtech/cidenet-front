import { useState, useEffect } from 'react';
import { BASE_URL } from '../helpers/constants'

const useFetch = (url) => {
    const [loader, setLoader] = useState(false);
    const [data, setData] = useState([]);
    const [auxData, setAuxData] = useState([]);

    useEffect(() => {
        if (!url) return;

        const fetchData = async () => {
            setLoader(true)
            const response = await fetch(`${BASE_URL}/${url}`);
            const data = await response.json();
            setData(data.data[url]);
            setAuxData(data.data[url])
            setLoader(false)
        };

        fetchData();
    }, [url]);

    return { data, loader, setData, auxData };
}
export default useFetch