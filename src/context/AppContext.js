import React from 'react';
import postService from '../api/postService';
import validateCreateEmployee from '../helpers/validations/validateCreateEmployee';
import useFetch from '../hooks/useFetch';
import useValidation from '../hooks/useValidation';

export const AppContext = React.createContext(null);

export const ContextWrapper = (props) => {
    const { data, loader, setData, auxData } = useFetch('employees');

    const INITIAL_STATE = {
        firstName: '',
        surname: '',
        secondSurname: '',
        otherName: '',
        country: "1",
        typeDocument: "1",
        document: '',
        area: "1",
        dateAccess: '',
        status: 1
    }
    const {
        values,
        errors,
        setValues,
        handleSubmit,
        handleChange
    } = useValidation(INITIAL_STATE, validateCreateEmployee, handleCreateEmployee);

    async function handleCreateEmployee () {
        try {
            const result = await postService(values, 'employees');
            if (!result.success) {
                alert(result.message)
                return
            }
            setValues(INITIAL_STATE)
            setData([result.data.data, ...data])
        } catch (error) {
            alert("Hubo un error")
        }
    }
    return (
        <AppContext.Provider value={{ data, loader, setData, auxData, values, setValues, errors, handleSubmit, handleChange }}>
            {props.children}
        </AppContext.Provider>
    );
}