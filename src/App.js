import React, { useState, useContext } from 'react';
import './App.css';
import Button from './components/Button';
import Header from './components/Header'
import ListEmployee from './components/ListEmployee';
import { AppContext } from './context/AppContext'
import Sidebar from './components/Sidebar';
import ModalEmployee from './components/ModalEmployee';

function App () {
  const [show, setShow] = useState(false)
  const { data, loader } = useContext(AppContext);
  return (
    <>
      <Header />
      <section className="hero">
        <div className="hero__info">
          <h1>Empleados</h1>
          <Button handler={() => setShow(true)}>Crear nuevo empleado</Button>
        </div>
      </section>

      <section className="main">
        <div className="main__sidebar">
          <Sidebar />
        </div>
        <div className="main__employees">
          <ListEmployee data={data} loader={loader} />
        </div>
      </section>

      <ModalEmployee show={show} setShow={setShow} />
    </>
  );
}

export default App;
