export const country = {
    colombia: 1,
    usa: 2
}
export const domains = {
    colombia: 'cidenet.com.co',
    usa: 'cidenet.com.us'
}
export const countries = [
    {
        id: country.colombia,
        name: 'Colombia'
    },
    {
        id: country.usa,
        name: 'Estados Unidos'
    },
]

export const typeDocuments = [
    {
        id: 1,
        name: 'Cédula de Ciudadanía'
    },
    {
        id: 2,
        name: 'Cédula de Extranjería'
    },
    {
        id: 3,
        name: 'Pasaporte'
    },
    {
        id: 4,
        name: 'Permiso Especial'
    }
]

export const areas = [
    {
        id: -1,
        name: 'Todas',
        icon: 'https://image.flaticon.com/icons/png/512/814/814513.png'
    },
    {
        id: 1,
        name: 'Administración',
        icon: 'https://image.flaticon.com/icons/png/512/2721/2721282.png'
    },
    {
        id: 2,
        name: 'Financiera',
        icon: 'https://image.flaticon.com/icons/png/512/2850/2850491.png'
    },
    {
        id: 3,
        name: 'Compras',
        icon: 'https://image.flaticon.com/icons/png/512/1019/1019607.png'
    },
    {
        id: 4,
        name: 'Infraestructura',
        icon: 'https://image.flaticon.com/icons/png/512/2790/2790156.png'
    },
    {
        id: 5,
        name: 'Operación',
        icon: 'https://image.flaticon.com/icons/png/512/993/993855.png'
    },
    {
        id: 6,
        name: 'Talento Humano',
        icon: 'https://image.flaticon.com/icons/png/512/3306/3306429.png'
    },
    {
        id: 7,
        name: 'Servicios Varios',
        icon: 'https://image.flaticon.com/icons/png/512/124/124559.png'
    }
]

export const allData = {
    country: countries,
    typeDocument: typeDocuments,
    area: areas
}

export const BASE_URL = 'http://localhost:19500/api/v1'

export const regexOnlyLetters = /^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$/i
export const maxCantLetters = 20