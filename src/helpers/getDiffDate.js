export const getDiffDate = (date) => {
    const dateIni = new Date(date);
    const dateFin = new Date();

    const diff = dateFin - dateIni;

    const diferenciaDias = Math.floor(diff / (1000 * 60 * 60 * 24));
    return diferenciaDias
}