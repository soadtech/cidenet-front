import { maxCantLetters, regexOnlyLetters } from "../constants";
import { formatDate } from "../formDate";
import { getDiffDate } from "../getDiffDate";

const maxCantLetter = 50;
const maxDaysAvaible = 31
var regularExp = /^\w+$/

export default function validateCreateEmployee (values) {
    let errores = {};
    const today = formatDate(new Date(), 'YYYY-MM-DD')

    if (!values.firstName) {
        errores.firstName = "El nombre es obligatorio";
    } else if (!regexOnlyLetters.test(values.firstName)) {
        errores.firstName = "Nombre no valido";
    } else if (values.firstName.trim().length > maxCantLetters) {
        errores.firstName = "El nombre no puede ser mayor a " + maxCantLetters;
    }

    if (!values.surname) {
        errores.surname = "El apellido es obligatorio";
    } else if (!regexOnlyLetters.test(values.surname)) {
        errores.surname = "Apellido no valido";
    } else if (values.surname.trim().length > maxCantLetters) {
        errores.surname = "El apellido no puede ser mayor a " + maxCantLetters;
    }


    if (!values.secondSurname) {
        errores.secondSurname = "El apellido es obligatorio";
    } else if (!regexOnlyLetters.test(values.secondSurname)) {
        errores.secondSurname = "Apellido no valido";
    } else if (values.secondSurname.trim().length > maxCantLetters) {
        errores.secondSurname = "El apellido no puede ser mayor a " + maxCantLetters;
    }

    if (values.otherName && !regexOnlyLetters.test(values.otherName)) {
        errores.otherName = "nombre no valido";
    } else if (values.otherName && values.otherName.trim().length > maxCantLetter) {
        errores.otherName = "El nombre no puede ser mayor a " + maxCantLetter;
    }

    if (!values.document) {
        errores.document = "El documento es obligatorio";
    } else if (!regularExp.test(values.document)) {
        errores.document = "documento no valido";
    } else if (values.document.trim().length > maxCantLetters) {
        errores.document = "El documento no puede ser mayor a " + maxCantLetters;
    }

    if (!values.dateAccess) {
        errores.dateAccess = "La fecha es obligatoria";
    } else if (values.dateAccess > today) {
        errores.dateAccess = "La fecha no puede ser mayor a la fecha actual";
    } else if (getDiffDate(values.dateAccess) > maxDaysAvaible) {
        errores.dateAccess = `La fecha no puede ser menor a la ${maxDaysAvaible} dias`;
    }

    return errores;
}