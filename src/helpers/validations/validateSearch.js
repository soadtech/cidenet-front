
export default function validateSearch (values) {
    let errores = {};

    if (!values.boxSearch) {
        errores.boxSearch = "El campo es obligatorio";
    }

    return errores;
}