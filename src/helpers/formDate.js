export const formatDate = (date, format) => {
    let newDate = '';
    switch (format) {
        case 'YYYY-MM-DD HH:mm:ss a': {
            const dateTimeFormat = new Intl.DateTimeFormat('en', {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit',
                hour: '2-digit',
                minute: '2-digit',
            });
            const [
                { value: month },
                ,
                { value: day },
                ,
                { value: year },
                ,
                { value: hour },
                ,
                { value: minute },
            ] = dateTimeFormat.formatToParts(date);
            const hour12 = date
                .toLocaleString('en-US', { hour: 'numeric', hour12: true })
                .split(' ')[1];

            newDate = `${year}-${month.length === 1 ? 0 + month : month}-${day.length === 1 ? 0 + day : day
                } ${hour}:${minute} ${hour12}`;

            return newDate;
        }
        case 'YYYY-MM-DD': {
            const dateTimeFormat = new Intl.DateTimeFormat('en', {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit',
                hour: '2-digit',
                minute: '2-digit',
            });
            const [
                { value: month },
                ,
                { value: day },
                ,
                { value: year }
            ] = dateTimeFormat.formatToParts(date);

            newDate = `${year}-${month.length === 1 ? 0 + month : month}-${day.length === 1 ? 0 + day : day
                }`;

            return newDate;
        }
        case 'obj': {
            const fechaPartes = date.split("-")
            newDate = new Date(fechaPartes[0], (fechaPartes[1] - 1), fechaPartes[2])
            return newDate;
        }
        default: {
            return 'Formato no valido';
        }
    }
};