import React from 'react'
import Error from '../Error'
import './styles.css'

export default function DatePicker ({ value, onChange, name, label, errors, ...rest }) {
    return (
        <div>
            <label>{label}</label>
            <div>
                <input value={value} name={name} onChange={onChange} className="datePicker" type="date" />
            </div>
            {errors[name] && <Error>{errors[name]}</Error>}
        </div>
    )
}
