import React from 'react'
import BoxSearch from '../BoxSearch'
import './styles.css'

export default function Header () {
    return (
        <header className="header">
            <h2>Logo</h2>
            <BoxSearch />
            <h2>Perfil</h2>
        </header>
    )
}
