import React from 'react'

export default function Error ({ children }) {
    return (
        <span style={{ color: 'red', fontWeight: 'bold', fontSize: 12 }}>{children}</span>
    )
}
