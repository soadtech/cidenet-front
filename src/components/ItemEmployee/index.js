import React, { useState, useContext } from 'react';
import { delService } from '../../api/postService';
import { AppContext } from '../../context/AppContext';
import Button from '../Button';
import Modal from '../Modal';
import ModalEmployee from '../ModalEmployee';
import './styles.css'

export default function ItemEmployee ({ firstName, area, id, employee }) {
    const [show, setShow] = useState(false)
    const [show1, setShow1] = useState(false)
    const { data, setData, setValues } = useContext(AppContext)

    const handleDelete = (idEmployee) => {
        try {
            delService(`employees/${idEmployee}`)
            setData(data.filter(employee => employee.document !== idEmployee))
            setShow(false)
        } catch (error) {
            alert("Ocurrio un error")
        }
    }
    const handleEdit = (employee) => {
        setValues(employee)
        setShow1(true)
    }
    return (
        <div className="item__employee">
            <div className="item__employee--profile">
                <img className="item__employee--img" src="https://i.ibb.co/g3qbhkY/036-person.png" alt='profile' />
                <div className="item__employee--data">
                    <p className="title">{firstName}</p>
                    <span className="subtitle">{area}</span>
                    <p className="title mt-15">Estado</p>
                    <span className="subtitle">{employee.status == 1 ? 'Activo' : 'Inactivo'}</span>
                </div>
            </div>
            <div>
                <p className="title">Hora de ingreso</p>
                <span className="subtitle">17/07/2021</span>
                <p className="title mt-15">Hora de salida</p>
                <span className="subtitle">No registra</span>
            </div>
            <div>
                <p className="title">Correo electrónico</p>
                <span className="subtitle">{employee.email}</span>
                <p className="title mt-15">Pais</p>
                <span className="subtitle">{employee.country.name}</span>
            </div>
            <div style={{ display: 'flex', justifyContent: 'space-evenly' }}>
                <Button handler={() => handleEdit(employee)} outline className="subtitle">Editar</Button>
                <Button handler={() => setShow(true)} className="subtitle">Delete</Button>
            </div>

            <Modal show={show}>
                <div>
                    <h1 className="text-center">¿Está seguro de que desea eliminar el empleado?</h1>
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-evenly', marginTop: 20 }}>
                    <Button outline handler={() => setShow(false)}>No</Button>
                    <Button handler={() => handleDelete(id)}>Si, eliminar</Button>
                </div>
            </Modal>
            <ModalEmployee show={show1} setShow={setShow1} editing />
        </div>
    )
}
