import React from 'react'
import Error from '../Error'
import './styles.css'

export default function Input ({ value, label, errors, name, onChange }) {

    return (
        <div>
            <label>{label}</label>
            <div>
                <input name={name} value={value} onChange={onChange} className='input' />
            </div>
            {errors[name] && <Error>{errors[name]}</Error>}
        </div>
    )
}
