import React from 'react'
import './styles.css'

export default function Select ({ options = [], label, ...rest }) {
    return (
        <div style={{ width: '40%' }}>
            <label>{label}</label>
            <div style={{ width: '100%' }}>
                <select className="select" {...rest}>
                    {options.map(option => (
                        <option key={option.id} value={option.id}>{option.name}</option>
                    ))}
                </select>
            </div>
        </div>
    )
}
