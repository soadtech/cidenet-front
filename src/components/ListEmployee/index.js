import React from 'react'
import ItemEmployee from '../ItemEmployee'
import Loader from '../Loader'

export default function ListEmployee ({ data, loader }) {

    return (
        <>
            {loader ? <Loader /> : (
                <>
                    {data.length == 0 ? (
                        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <h5>No hay datos</h5>
                        </div>
                    ) : (
                        <>
                            {data.map(employee => (
                                <ItemEmployee
                                    employee={employee}
                                    id={employee.document}
                                    key={employee.document}
                                    firstName={employee.firstName}
                                    area={employee.area.name}
                                />
                            ))}
                        </>
                    )}
                </>
            )}
        </>
    )
}
