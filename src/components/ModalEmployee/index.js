import React, { useState, useContext } from 'react'
import { AppContext } from '../../context/AppContext';
import Button from '../Button';
import DatePicker from '../DatePicker';
import Input from '../Input';
import Modal from '../Modal';
import Select from '../Select';
import { typeDocuments, countries, areas } from '../../helpers/constants'

export default function ModalEmployee ({ show, setShow, editing }) {
    const { values, errors, handleSubmit, handleChange } = useContext(AppContext);

    const { firstName, surname, secondSurname, otherName, country, typeDocument, document, area, dateAccess } = values

    return (
        <Modal show={show}>
            <div>
                <h1 className="text-center">{editing ? "Editando empleado" : "Creando un nuevo empleado"}</h1>
            </div>

            <div style={{ marginTop: 10 }}>
                <h3>Informacion principal</h3>
                <hr />
            </div>
            <form onSubmit={handleSubmit}>
                <div style={{ display: 'flex', justifyContent: 'space-evenly', marginBottom: 10 }}>
                    <Input errors={errors} value={firstName} onChange={handleChange} name="firstName" label="Primer nombre*" />
                    <Input errors={errors} value={surname} onChange={handleChange} name="surname" label="Primer Apellido*" />
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-evenly', marginBottom: 10 }}>
                    <Input errors={errors} value={secondSurname} onChange={handleChange} name="secondSurname" label="Segundo Apellido*" />
                    <Input errors={errors} value={otherName} onChange={handleChange} name="otherName" label="Otros Nombres" />
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-evenly', marginBottom: 10 }}>
                    <Select value={country} onChange={handleChange} name="country" label="Pais*" options={countries} />
                    <Select value={typeDocument} onChange={handleChange} name="typeDocument" label="Tipo de identificacion*" options={typeDocuments} />
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-evenly', marginBottom: 10 }}>
                    <Input errors={errors} value={document} onChange={handleChange} name="document" label="Numero identificacion*" />
                </div>

                <div style={{ marginTop: 10 }}>
                    <h3>Informacion de ingreso</h3>
                    <hr />
                </div>

                <div style={{ display: 'flex', justifyContent: 'space-evenly', marginBottom: 10 }}>
                    <DatePicker errors={errors} value={dateAccess} name="dateAccess" onChange={handleChange} label="Fecha de ingreso*" />
                    <Select value={area} onChange={handleChange} name="area" label="Area*" options={areas} />
                </div>

            </form>

            <div style={{ display: 'flex', justifyContent: 'space-evenly' }}>
                <Button outline handler={() => setShow(false)}>Cancelar</Button>
                <Button handler={!editing ? handleSubmit : () => alert("Esta opcion no se termino")}>{editing ? 'Editar' : 'Crear'}</Button>
            </div>
        </Modal>
    )
}
