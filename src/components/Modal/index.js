import React from 'react';
import './styles.css'

export default function Modal ({ show, children, title }) {
    if (!show) return null
    return (
        <div className="modal">
            <div className="modal-content">
                <div className="modal-body">
                    {children}
                </div>
            </div>
        </div>
    )
}
