import React, { useContext } from 'react'
import { AppContext } from '../../context/AppContext';
import './styles.css'

export default function ItemArea ({ icon, label, id }) {
    const { auxData, setData } = useContext(AppContext);

    const handleFilter = (idArea) => {
        if (idArea == -1) {
            setData(auxData)
            return
        }
        setData(auxData.filter(employee => employee.area.id == idArea))
    }
    return (
        <div onClick={() => handleFilter(id)} className="item_area">
            <img className="item_area--icon" src={icon} alt='icon area' />
            <span className="item_area--label">{label}</span>
        </div>
    )
}
