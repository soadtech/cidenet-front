import React, { useContext, useState } from 'react'
import { getService } from '../../api/postService';
import { AppContext } from '../../context/AppContext';
import Button from '../Button';

export default function BoxSearch () {
    const [text, setText] = useState('')
    const { setData } = useContext(AppContext);
    async function handleSearch (e) {
        if (text.trim() === "") {
            alert('Ingrese una entrada')
            return
        }

        try {
            const result = await getService(`employees/filter?text=${text}`)
            if (!result.success) {
                alert(result.message)
                return
            }
            setData(result.data.filters)
        } catch (error) {
            alert("Ocurrio un error")
        }

    }
    return (
        <div style={{ display: 'flex', width: '100%' }}>
            <input
                placeholder="Primer Nombre, Otros Nombres, Primer Apellido, Segundo Apellido, Tipo de Identificación..."
                style={{ width: '80%', padding: '10px' }}
                className="input"
                name="boxSearch"
                onChange={(e) => setText(e.target.value)}
            />
            <Button handler={handleSearch}>Buscar</Button>
        </div>
    )
}
