import React from 'react'
import './styles.css'

export default function Button ({ children, handler, outline }) {
    return (
        <button className={`btn ${outline ? 'btn-outline' : ''}`} onClick={handler}>
            {children}
        </button>
    )
}
