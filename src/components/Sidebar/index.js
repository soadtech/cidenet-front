import React from 'react'
import { areas } from '../../helpers/constants'
import ItemArea from '../ItemArea'

export default function Sidebar () {
    return (
        <>
            <h2>Areas</h2>
            {areas.map(area => (
                <ItemArea key={area.id} id={area.id} icon={area.icon} label={area.name} />
            ))}
        </>
    )
}
